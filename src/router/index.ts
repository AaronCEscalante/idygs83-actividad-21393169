import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import GaleriaViewVue from '@/views/GaleriaView.vue'
import ProfileViewVue from '@/views/ProfileView.vue'
import AboutViewVue from '@/views/AboutView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/profile',
      name: 'profile',
      component: ProfileViewVue
    },
    {
      path: '/about',
      name: 'about',
      component: AboutViewVue
    },
    {
      path: '/galeria',
      name: 'galeria',
      component: GaleriaViewVue
    },
    {
      path: '/menu',
      name: 'menu',
      component: GaleriaViewVue
    },
  ]
})

export default router
